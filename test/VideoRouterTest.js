const VideoRouter = require('../src/VideoRouter');
const assert = require('assert');

describe('VideoRouter', () => {
    describe('#constructor', () => {
        it('should build without error', () => {
            new VideoRouter('https://www.example.com/abc123');
        });
    });

    describe('#getVideoId', () => {
        it('should return the video ID specified in the URL', () => {
            let router = new VideoRouter('https://www.example.com?v=abcvidID');
            let expectedId = 'abcvidID';
            let actualId = router.getVideoId();

            assert(expectedId == actualId, "Actual video ID is not equal to what was expected");
        });

        it('should return the video ID even when query params are present', () => {
            let router = new VideoRouter('https://www.example.com/?v=abcvidID&a=b&c=d');
            let expectedId = 'abcvidID';
            let actualId = router.getVideoId();

            assert(expectedId === actualId, "Actual video ID is not equal to what was expected");
        });
    });

    describe('#getTimestamps', () => {
        it('should return an array of desired timestamps from the URL', () => {
            let router = new VideoRouter('https://www.example.com/abcvidID?ts=0-10,20-30,50-80');
            let expectedTimestamps = [
                { start: 0, end: 10 },
                { start: 20, end: 30},
                { start: 50, end: 80}
            ];

            let actualTimestamps = router.getTimestamps();

            assert(expectedTimestamps.length == actualTimestamps.length, "The number of actual timestamps is different than expected");

            let numberOfDifferences = 0;
            for (let i = 0; i < expectedTimestamps.length; ++i) {
                let actual = actualTimestamps[i];
                let expected = expectedTimestamps[i];

                if (actual.start != expected.start || actual.end != expected.end)
                    ++numberOfDifferences;
            }

            assert(numberOfDifferences === 0, `Actual timestamps had ${numberOfDifferences} differences from the expected timestamps`);
        });

        it('should return an array of desired timestamps from the URL even with other parameters', () => {
            let router = new VideoRouter('https://www.example.com/abcvidID?ts=0-10,20-30,50-80&hello=world&test=good');
            let expectedTimestamps = [
                { start: 0, end: 10 },
                { start: 20, end: 30},
                { start: 50, end: 80}
            ];

            let actualTimestamps = router.getTimestamps();

            assert(expectedTimestamps.length == actualTimestamps.length, "The number of actual timestamps is different than expected");

            let numberOfDifferences = 0;
            for (let i = 0; i < expectedTimestamps.length; ++i) {
                let actual = actualTimestamps[i];
                let expected = expectedTimestamps[i];

                if (actual.start != expected.start || actual.end != expected.end)
                    ++numberOfDifferences;
            }

            assert(numberOfDifferences === 0, `Actual timestamps had ${numberOfDifferences} differences from the expected timestamps`);
        });

        it('should return null when the timestamp parameter is missing', () => {
            let router = new VideoRouter('https://www.example.com/abcvidID?');
            let actualTimestamps = router.getTimestamps();

            assert(actualTimestamps === null, "Return value was not null as expected");
        });
    })
});