const MiniExtractCssPlugin = require('mini-css-extract-plugin');
const HtmlPlugin = require('html-webpack-plugin');

module.exports = {
    entry: "./src/index.js",
    plugins: [
        new MiniExtractCssPlugin({
            filename: '[name].css',
        }),
        new HtmlPlugin({
            inject: false,
            template: 'src/index.template.html'
        })
    ],
    module: {
        rules: [
            {
                test: /\.css/,
                use: [
                    {
                        loader: MiniExtractCssPlugin.loader
                    },
                    'css-loader'
                ]
            }
        ]
    },
    devtool: 'eval'
};