
/**
 * Handles extracting video ID information from the URL.
 */
class VideoRouter {
    /**
     * @constructor
     */
    constructor(windowUrl) {
        this.windowUrl = windowUrl;
    }

    getVideoId() {
        var queryDictionary = this._extractQueryParams();

        if (queryDictionary === null || !queryDictionary.hasOwnProperty('v'))
            return null;

        return queryDictionary['v'];
    }

    getTimestamps() {
        var queryDictionary = this._extractQueryParams();

        if (queryDictionary === null || !queryDictionary.hasOwnProperty('ts'))
            return null;

        var timestampBlocks = queryDictionary['ts'].split(',');
        var timestamps = timestampBlocks.map((block) => {
            let blockSplit = block.split('-');
            let startTime = blockSplit[0];
            let endTime = blockSplit[1];

            return { start: startTime, end: endTime };
        });

        return timestamps;
    }

    _extractQueryParams() {
        let urlSplit = this.windowUrl.split('?');
        let queryParams = urlSplit.length > 1 ? urlSplit[1] : null;

        if (queryParams == null)
            return null;

        let querySections = queryParams.split('&');
        let queryDictionary = {};
        querySections.map((section) => {
            let sectionSplit = section.split('=');
            let key = sectionSplit[0];
            let value = sectionSplit[1];
            queryDictionary[key] = value;
        });

        return queryDictionary;
    }
}

module.exports = VideoRouter;