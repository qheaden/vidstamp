const knockout = require('knockout');
const thisize = require('../Thisize');
const VideoPlayer = require('../VideoPlayer');

class HomepageViewModel {
    constructor(containerElement, videoRouter) {
        this.videoRouter = videoRouter;
        this.isWatcher = knockout.observable(videoRouter.getVideoId() !== null && videoRouter.getTimestamps() !== null);

        thisize(
            this,
            [
                'createVideoPlayer'
            ]
        );

        knockout.applyBindings(this);

        if (this.isWatcher())
            this.createVideoPlayer();
    }

    createVideoPlayer() {
        this.videoPlayer = new VideoPlayer(
            window,
            'videoPlayer',
            this.videoRouter.getVideoId(),
            this.videoRouter.getTimestamps());
    }
}

module.exports = HomepageViewModel;