const thisize = require('./Thisize');

class VideoPlayer {
    constructor(window, playerFrameId, videoId, timestamps) {
        this.window = window;
        this.playerFrameId = playerFrameId,
        this.videoId = videoId;
        this.ytPlayer = null;
        this.timestamps = timestamps;
        this.currentTimestamp = 0;
        this.timestampUpdateIntervalId = null;

        thisize(
            this,
            [
                '_injectYoutubeApiScript',
                '_hookPlayerEvents',
                '_createYtPlayer',
                '_onApiReady',
                '_onYtPlayerReady',
                '_onYtPlayerStateChange',
                '_updateCurrentTimestamp'
            ]
        );

        this._injectYoutubeApiScript();
        this._hookPlayerEvents();
    }

    _injectYoutubeApiScript() {
        var script = this.window.document.createElement('script');
        script.src = "https://www.youtube.com/iframe_api";
        var firstScriptTag = this.window.document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(script, firstScriptTag);
    }

    _hookPlayerEvents() {
        this.window.onYouTubeIframeAPIReady = this._onApiReady;
    }

    _createYtPlayer() {
        this.ytPlayer = new this.window.YT.Player(this.playerFrameId, {
            videoId: this.videoId,
            events: {
                'onReady': this._onYtPlayerReady,
                'onStateChange': this._onYtPlayerStateChange
            }
        });
    }

    _updateCurrentTimestamp() {
        if (this.ytPlayer === null || !this.ytPlayer.hasOwnProperty('getCurrentTime'))
            return;

        this.currentTimestamp = this.ytPlayer.getCurrentTime();
        this._syncVideoWithTimestampBlock();
    }

    _syncVideoWithTimestampBlock() {
        if (this.timestamps === null)
            return;

        // Search for which timestamp block the video should be within.
        let nextBlock = null;
        for (let block of this.timestamps) {
            if (block.end <= this.currentTimestamp)
                continue;

            nextBlock = block;
            break;
        }

        // If we couldn't find the next block, that means that the video has reached the
        // end of it's final block and it is time to be stopped.
        if (nextBlock === null) {
            this.ytPlayer.stopVideo();
            return;
        }

        // If we are already playing within the correct block, don't
        // seek the video.
        if (this.currentTimestamp >= nextBlock.start)
            return;

        // If we discover it is time to go to another block, we update the timestamp
        // and seek the video to the new timestamp.
        this.currentTimestamp = nextBlock.start;
        this.ytPlayer.seekTo(this.currentTimestamp, true);
    }

    // YouTube IFrame player events.
    _onApiReady() {
        this._createYtPlayer();
    }

    _onYtPlayerReady() {
        this.ytPlayer.playVideo();
    }

    _onYtPlayerStateChange(event) {
        if (
            (event.data === this.window.YT.PlayerState.ENDED || event.data === -1)
            && this.timestampUpdateIntervalId !== null
        ) {
            clearInterval(this.timestampUpdateIntervalId);
            this.timestampUpdateIntervalId = null;
        } else if (event.data === this.window.YT.PlayerState.PLAYING && this.timestampUpdateIntervalId === null) {
            this._updateCurrentTimestamp();
            this.timestampUpdateIntervalId = setInterval(this._updateCurrentTimestamp, 500);
        }
    }
}

module.exports = VideoPlayer;