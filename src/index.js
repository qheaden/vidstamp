require('./site.css');
const VideoRouter = require('./VideoRouter');
const HomepageViewModel = require('./view/HomepageViewModel');

function appMain() {
    let videoRouter = new VideoRouter(window.location.href);
    new HomepageViewModel(window.document.body, videoRouter);
}

window.addEventListener('load', () => {
    appMain();
});