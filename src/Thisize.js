/**
 * A utility function to bind all specified methods of an object to
 * its own instance.
 */
function thisize(object, methodNames) {
    methodNames.map((name) => {
        object[name] = object[name].bind(object);
    });
}

module.exports = thisize;